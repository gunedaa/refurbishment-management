# Refurbishment Management

This is a "Refurbishment Management" app which enables managing the different phases of a refurbishment process by 5 interfaces: General Overview, IGT, MR, CT and SUR. 

General Overview interface displays all modelities with the entered dates such as expected, started, finished or delayed in different colors for the phases of this refurbishment such as stripping, cleaning, painting etc.

The rest of the interfaces display only the modelity that they are called. It is possible to use these interfaces to add a new date for a phase of this refurbishment by clicking on the cell and choosing a date from a dateTimePicker and a state from a combobox.
