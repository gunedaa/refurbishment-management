﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RefurbishmentManagement
{
    public partial class Welcome : MetroFramework.Forms.MetroForm
    {
        public Welcome()
        {
            InitializeComponent();
        }

        private void btnGeneralOverview_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            GeneralOverview overview = new GeneralOverview();
            overview.Show();
        }

        private void btnIGT_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            IGT igt = new IGT();
            igt.Show();
        }

        private void btnAddOrUpdate_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            AddOrUpdateRecord updForm = new AddOrUpdateRecord();
            updForm.Show();
        }
    }
}
