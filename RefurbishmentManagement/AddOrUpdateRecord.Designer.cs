﻿namespace RefurbishmentManagement
{
    partial class AddOrUpdateRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.panel20 = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnAddNewRecord = new System.Windows.Forms.Button();
            this.cbModelity = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbType = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbProjectName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbSalesNr = new System.Windows.Forms.TextBox();
            this.btnTurnBack = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.button14 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.tbRemoveSalesNr = new System.Windows.Forms.TextBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Location = new System.Drawing.Point(25, 313);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(87, 1);
            this.panel1.TabIndex = 27;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Black;
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.button5);
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(225, 1);
            this.panel6.TabIndex = 6;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Black;
            this.panel7.Location = new System.Drawing.Point(-86, -11);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(225, 1);
            this.panel7.TabIndex = 4;
            // 
            // button5
            // 
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button5.Location = new System.Drawing.Point(-68, -11);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(378, 23);
            this.button5.TabIndex = 3;
            this.button5.Text = "Register A Request";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button6.Location = new System.Drawing.Point(18, 0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(378, 23);
            this.button6.TabIndex = 5;
            this.button6.Text = "Register A Request";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Black;
            this.panel8.Location = new System.Drawing.Point(-86, -11);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(225, 1);
            this.panel8.TabIndex = 4;
            // 
            // button7
            // 
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button7.Location = new System.Drawing.Point(-68, -11);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(378, 23);
            this.button7.TabIndex = 3;
            this.button7.Text = "Register A Request";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel17);
            this.groupBox1.Controls.Add(this.btnAddNewRecord);
            this.groupBox1.Controls.Add(this.cbModelity);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbProjectName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbSalesNr);
            this.groupBox1.Location = new System.Drawing.Point(23, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(257, 160);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add New Record";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Black;
            this.panel17.Controls.Add(this.panel18);
            this.panel17.Controls.Add(this.button17);
            this.panel17.Controls.Add(this.panel20);
            this.panel17.Controls.Add(this.button18);
            this.panel17.Location = new System.Drawing.Point(143, 131);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(87, 1);
            this.panel17.TabIndex = 32;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Black;
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Controls.Add(this.button16);
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(225, 1);
            this.panel18.TabIndex = 6;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Black;
            this.panel19.Location = new System.Drawing.Point(-86, -11);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(225, 1);
            this.panel19.TabIndex = 4;
            // 
            // button16
            // 
            this.button16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button16.Location = new System.Drawing.Point(-68, -11);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(378, 23);
            this.button16.TabIndex = 3;
            this.button16.Text = "Register A Request";
            this.button16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button17.FlatAppearance.BorderSize = 0;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button17.Location = new System.Drawing.Point(18, 0);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(378, 23);
            this.button17.TabIndex = 5;
            this.button17.Text = "Register A Request";
            this.button17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button17.UseVisualStyleBackColor = true;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Black;
            this.panel20.Location = new System.Drawing.Point(-86, -11);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(225, 1);
            this.panel20.TabIndex = 4;
            // 
            // button18
            // 
            this.button18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button18.FlatAppearance.BorderSize = 0;
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button18.Location = new System.Drawing.Point(-68, -11);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(378, 23);
            this.button18.TabIndex = 3;
            this.button18.Text = "Register A Request";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button18.UseVisualStyleBackColor = true;
            // 
            // btnAddNewRecord
            // 
            this.btnAddNewRecord.BackgroundImage = global::RefurbishmentManagement.Properties.Resources.arrowblack1;
            this.btnAddNewRecord.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAddNewRecord.FlatAppearance.BorderSize = 0;
            this.btnAddNewRecord.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddNewRecord.Font = new System.Drawing.Font("Rockwell", 9F);
            this.btnAddNewRecord.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAddNewRecord.Location = new System.Drawing.Point(143, 134);
            this.btnAddNewRecord.Name = "btnAddNewRecord";
            this.btnAddNewRecord.Size = new System.Drawing.Size(135, 23);
            this.btnAddNewRecord.TabIndex = 31;
            this.btnAddNewRecord.Text = "Add";
            this.btnAddNewRecord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddNewRecord.UseVisualStyleBackColor = true;
            this.btnAddNewRecord.Click += new System.EventHandler(this.button19_Click);
            // 
            // cbModelity
            // 
            this.cbModelity.FormattingEnabled = true;
            this.cbModelity.Items.AddRange(new object[] {
            "IGT",
            "CT",
            "MR",
            "SUR"});
            this.cbModelity.Location = new System.Drawing.Point(108, 97);
            this.cbModelity.Name = "cbModelity";
            this.cbModelity.Size = new System.Drawing.Size(121, 21);
            this.cbModelity.TabIndex = 37;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 36;
            this.label3.Text = "Modelity:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Type:";
            // 
            // tbType
            // 
            this.tbType.Location = new System.Drawing.Point(108, 71);
            this.tbType.Name = "tbType";
            this.tbType.Size = new System.Drawing.Size(121, 20);
            this.tbType.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Project Name:";
            // 
            // tbProjectName
            // 
            this.tbProjectName.Location = new System.Drawing.Point(108, 45);
            this.tbProjectName.Name = "tbProjectName";
            this.tbProjectName.Size = new System.Drawing.Size(121, 20);
            this.tbProjectName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Sales Nr:";
            // 
            // tbSalesNr
            // 
            this.tbSalesNr.Location = new System.Drawing.Point(108, 19);
            this.tbSalesNr.Name = "tbSalesNr";
            this.tbSalesNr.Size = new System.Drawing.Size(121, 20);
            this.tbSalesNr.TabIndex = 0;
            // 
            // btnTurnBack
            // 
            this.btnTurnBack.BackgroundImage = global::RefurbishmentManagement.Properties.Resources.left_arrow_black_512;
            this.btnTurnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTurnBack.FlatAppearance.BorderSize = 0;
            this.btnTurnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTurnBack.Font = new System.Drawing.Font("Rockwell", 9F);
            this.btnTurnBack.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnTurnBack.Location = new System.Drawing.Point(-91, 317);
            this.btnTurnBack.Name = "btnTurnBack";
            this.btnTurnBack.Size = new System.Drawing.Size(251, 23);
            this.btnTurnBack.TabIndex = 26;
            this.btnTurnBack.Text = "                                            Turn back";
            this.btnTurnBack.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTurnBack.UseVisualStyleBackColor = true;
            this.btnTurnBack.Click += new System.EventHandler(this.btnTurnBack_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.panel13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.tbRemoveSalesNr);
            this.groupBox3.Controls.Add(this.btnRemove);
            this.groupBox3.Location = new System.Drawing.Point(23, 226);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(256, 78);
            this.groupBox3.TabIndex = 36;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Remove a Record:";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Black;
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Controls.Add(this.button13);
            this.panel13.Controls.Add(this.panel16);
            this.panel13.Controls.Add(this.button14);
            this.panel13.Location = new System.Drawing.Point(143, 48);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(87, 1);
            this.panel13.TabIndex = 30;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Black;
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Controls.Add(this.button12);
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(225, 1);
            this.panel14.TabIndex = 6;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Black;
            this.panel15.Location = new System.Drawing.Point(-86, -11);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(225, 1);
            this.panel15.TabIndex = 4;
            // 
            // button12
            // 
            this.button12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button12.Location = new System.Drawing.Point(-68, -11);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(378, 23);
            this.button12.TabIndex = 3;
            this.button12.Text = "Register A Request";
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button13.Location = new System.Drawing.Point(18, 0);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(378, 23);
            this.button13.TabIndex = 5;
            this.button13.Text = "Register A Request";
            this.button13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.UseVisualStyleBackColor = true;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Black;
            this.panel16.Location = new System.Drawing.Point(-86, -11);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(225, 1);
            this.panel16.TabIndex = 4;
            // 
            // button14
            // 
            this.button14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button14.Location = new System.Drawing.Point(-68, -11);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(378, 23);
            this.button14.TabIndex = 3;
            this.button14.Text = "Register A Request";
            this.button14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button14.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(26, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Sales Nr:";
            // 
            // tbRemoveSalesNr
            // 
            this.tbRemoveSalesNr.Location = new System.Drawing.Point(108, 19);
            this.tbRemoveSalesNr.Name = "tbRemoveSalesNr";
            this.tbRemoveSalesNr.Size = new System.Drawing.Size(121, 20);
            this.tbRemoveSalesNr.TabIndex = 0;
            // 
            // btnRemove
            // 
            this.btnRemove.BackgroundImage = global::RefurbishmentManagement.Properties.Resources.arrowblack1;
            this.btnRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRemove.FlatAppearance.BorderSize = 0;
            this.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemove.Font = new System.Drawing.Font("Rockwell", 9F);
            this.btnRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRemove.Location = new System.Drawing.Point(143, 49);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(135, 23);
            this.btnRemove.TabIndex = 31;
            this.btnRemove.Text = "Remove";
            this.btnRemove.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // AddOrUpdateRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 345);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnTurnBack);
            this.Controls.Add(this.groupBox3);
            this.Name = "AddOrUpdateRecord";
            this.Text = "Add/Remove a Record";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AddOrUpdateRecord_FormClosed);
            this.Load += new System.EventHandler(this.AddOrUpdateRecord_Load);
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btnTurnBack;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbProjectName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbSalesNr;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnAddNewRecord;
        private System.Windows.Forms.ComboBox cbModelity;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbRemoveSalesNr;
        private System.Windows.Forms.Button btnRemove;
    }
}