﻿namespace RefurbishmentManagement
{
    partial class SUR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnTurnBack = new System.Windows.Forms.Button();
            this.panel46 = new System.Windows.Forms.Panel();
            this.panel47 = new System.Windows.Forms.Panel();
            this.panel48 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel49 = new System.Windows.Forms.Panel();
            this.button28 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.btnAddNewRecord = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.Location = new System.Drawing.Point(20, 60);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1516, 558);
            this.dataGridView1.TabIndex = 31;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellStyleChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellStyleChanged);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView1_ColumnWidthChanged);
            this.dataGridView1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dataGridView1_Scroll);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnTurnBack);
            this.groupBox1.Controls.Add(this.panel46);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.btnAddNewRecord);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(20, 618);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(1516, 59);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            // 
            // btnTurnBack
            // 
            this.btnTurnBack.BackgroundImage = global::RefurbishmentManagement.Properties.Resources.left_arrow_black_512;
            this.btnTurnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTurnBack.FlatAppearance.BorderSize = 0;
            this.btnTurnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTurnBack.Font = new System.Drawing.Font("Rockwell", 9F);
            this.btnTurnBack.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnTurnBack.Location = new System.Drawing.Point(-104, 23);
            this.btnTurnBack.Name = "btnTurnBack";
            this.btnTurnBack.Size = new System.Drawing.Size(251, 23);
            this.btnTurnBack.TabIndex = 24;
            this.btnTurnBack.Text = "                                            Turn back";
            this.btnTurnBack.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTurnBack.UseVisualStyleBackColor = true;
            this.btnTurnBack.Click += new System.EventHandler(this.btnTurnBack_Click);
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.Black;
            this.panel46.Controls.Add(this.panel47);
            this.panel46.Controls.Add(this.button1);
            this.panel46.Controls.Add(this.panel49);
            this.panel46.Controls.Add(this.button28);
            this.panel46.Location = new System.Drawing.Point(1222, 21);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(240, 1);
            this.panel46.TabIndex = 26;
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.Color.Black;
            this.panel47.Controls.Add(this.panel48);
            this.panel47.Controls.Add(this.button8);
            this.panel47.Location = new System.Drawing.Point(0, 0);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(225, 1);
            this.panel47.TabIndex = 6;
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.Black;
            this.panel48.Location = new System.Drawing.Point(-86, -11);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(225, 1);
            this.panel48.TabIndex = 4;
            // 
            // button8
            // 
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button8.Location = new System.Drawing.Point(-68, -31);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(378, 43);
            this.button8.TabIndex = 3;
            this.button8.Text = "Register A Request";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button1.Location = new System.Drawing.Point(18, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(378, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Register A Request";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.Black;
            this.panel49.Location = new System.Drawing.Point(-86, -11);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(225, 1);
            this.panel49.TabIndex = 4;
            // 
            // button28
            // 
            this.button28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button28.FlatAppearance.BorderSize = 0;
            this.button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button28.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button28.Location = new System.Drawing.Point(-68, -11);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(378, 23);
            this.button28.TabIndex = 3;
            this.button28.Text = "Register A Request";
            this.button28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button28.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Location = new System.Drawing.Point(13, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(87, 1);
            this.panel1.TabIndex = 25;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Black;
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.button5);
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(225, 1);
            this.panel6.TabIndex = 6;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Black;
            this.panel7.Location = new System.Drawing.Point(-86, -11);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(225, 1);
            this.panel7.TabIndex = 4;
            // 
            // button5
            // 
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button5.Location = new System.Drawing.Point(-68, -11);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(378, 23);
            this.button5.TabIndex = 3;
            this.button5.Text = "Register A Request";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button6.Location = new System.Drawing.Point(18, 0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(378, 23);
            this.button6.TabIndex = 5;
            this.button6.Text = "Register A Request";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Black;
            this.panel8.Location = new System.Drawing.Point(-86, -11);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(225, 1);
            this.panel8.TabIndex = 4;
            // 
            // button7
            // 
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button7.Location = new System.Drawing.Point(-68, -11);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(378, 23);
            this.button7.TabIndex = 3;
            this.button7.Text = "Register A Request";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.UseVisualStyleBackColor = true;
            // 
            // btnAddNewRecord
            // 
            this.btnAddNewRecord.BackgroundImage = global::RefurbishmentManagement.Properties.Resources.arrowblack;
            this.btnAddNewRecord.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAddNewRecord.FlatAppearance.BorderSize = 0;
            this.btnAddNewRecord.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddNewRecord.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAddNewRecord.Location = new System.Drawing.Point(1220, 23);
            this.btnAddNewRecord.Name = "btnAddNewRecord";
            this.btnAddNewRecord.Size = new System.Drawing.Size(446, 27);
            this.btnAddNewRecord.TabIndex = 27;
            this.btnAddNewRecord.Text = "Add/Remove a Record";
            this.btnAddNewRecord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddNewRecord.UseVisualStyleBackColor = true;
            this.btnAddNewRecord.Click += new System.EventHandler(this.btnAddNewRecord_Click);
            // 
            // SUR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1556, 697);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Name = "SUR";
            this.Text = "SUR";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SUR_FormClosed);
            this.Load += new System.EventHandler(this.SUR_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnTurnBack;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btnAddNewRecord;
    }
}