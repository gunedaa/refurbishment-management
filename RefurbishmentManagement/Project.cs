﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefurbishmentManagement
{
    public class Project
    {
        public string Type { get; set; }
        public int ProjectNr { get; set; }
    }
}
