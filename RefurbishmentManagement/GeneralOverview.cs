﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RefurbishmentManagement
{
    public partial class GeneralOverview : MetroFramework.Forms.MetroForm
    {
        List<Project> Projects;

        public GeneralOverview()
        {
            InitializeComponent();
            Projects = new List<Project>();
        }
        
        private void GeneralOverview_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void GetAllRunningProjects()
        {
            try
            {
                // get lines from the text file
                string[] lines = File.ReadAllLines(@"..\..\AllRefurbishment.txt");

                List<string> allProjects = new List<string>();

                for (int i = 0; i < lines.Length; i++)
                {
                    string[] values = lines[i].ToString().Split(',');
                    if (values[0] == string.Empty)
                        continue;
                    Project currentProject = new Project();
                    currentProject.Type = values[0];
                    currentProject.ProjectNr = Convert.ToInt32(values[1]);
                    Projects.Add(currentProject);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnIGT_Click(object sender, EventArgs e)
        {
            IGT igt = new IGT();
            igt.Show();
            this.Visible = false;
        }

        private void GeneralOverview_Load(object sender, EventArgs e)
        {
            try
            {
                GetAllRunningProjects();
                int nr = 0;
                foreach (Project p in Projects)
                {
                    if (string.Compare(p.Type, "IGT") == 0)
                    {
                        ImportIGT(p.ProjectNr.ToString(), nr);
                    }
                    else if (string.Compare(p.Type, "MR") == 0)
                    {
                        ImportMR(p.ProjectNr.ToString(), nr);
                    }
                    else if (string.Compare(p.Type, "CT") == 0)
                    {
                        ImportCT(p.ProjectNr.ToString(), nr);
                    }
                    else if (string.Compare(p.Type, "SUR") == 0)
                    {
                        ImportSUR(p.ProjectNr.ToString(), nr);
                    }
                    nr++;
                }

                WindowState = FormWindowState.Maximized;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void ImportIGT(string fileName, int projectNr)
        {
            try
            {
                // get lines from the text file
                string[] lines = File.ReadAllLines($"../../{fileName}.txt");
                string[] values;
                List<DateTime> expected = new List<DateTime>();
                List<DateTime> started = new List<DateTime>();
                List<DateTime> finished = new List<DateTime>();
                List<DateTime> delayed = new List<DateTime>();
                for (int i = 0; i < lines.Length; i++)
                {
                    values = lines[i].ToString().Split(',', '=');
                    string[] row = new string[12];
                    int stripping = -1;
                    int cleaning = -1;
                    int paint = -1;
                    int cabinet = -1;
                    int table = -1;
                    int stand = -1;
                    int display = -1;
                    int ucontrols = -1;
                    int test = -1;
                    int prepack = -1;
                    int endpack = -1;
                    int release = -1;
                    bool thereIsAStarted = false;

                    for (int j = 0; j < values.Length; j++)
                    {
                        row[0] = values[0].Trim();
                        row[1] = values[1].Trim();
                        row[2] = values[2].Trim();
                        row[3] = values[3].Trim();
                        if (values[j] == "Stripping_e")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_s")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_f")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_d")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_e")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim();
                                paint = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim();
                                paint = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim(); paint = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim(); paint = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Cabinet_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                expected.Add(temp.Date);
                                cabinet = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Cabinet_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                started.Add(temp.Date);
                                cabinet = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Cabinet_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                finished.Add(temp.Date);
                                cabinet = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Cabinet_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                delayed.Add(temp.Date);
                                cabinet = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Table_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                expected.Add(temp.Date);
                                table = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Table_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                started.Add(temp.Date);
                                table = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Table_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                finished.Add(temp.Date);
                                table = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Table_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                delayed.Add(temp.Date);
                                table = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Stand_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                expected.Add(temp.Date);
                                stand = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Stand_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                started.Add(temp.Date);
                                stand = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Stand_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                finished.Add(temp.Date);
                                stand = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Stand_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                delayed.Add(temp.Date);
                                stand = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Display_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                expected.Add(temp.Date);
                                display = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Display_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                started.Add(temp.Date);
                                display = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Display_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                finished.Add(temp.Date);
                                display = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Display_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                delayed.Add(temp.Date);
                                display = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "UControls_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                ucontrols = 0;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                expected.Add(temp.Date);
                            }
                            if (cabinet == 0 || table == 0 || stand == 0 || display == 0 || ucontrols == 0)
                            {
                                DateTime latestDate = expected.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "UControls_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                ucontrols = 2;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                finished.Add(temp.Date);
                            }
                            if ((cabinet == 2 || table == 2 || stand == 2 || display == 2 || ucontrols == 2) && thereIsAStarted == false) //if one is finished, display started (with the finished date)
                            {
                                DateTime latestDate = finished.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            if (cabinet == 2 && table == 2 && stand == 2 && display == 2 && ucontrols == 2) //only when all of them are finished, display finished
                            {
                                DateTime latestDate = finished.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "UControls_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                ucontrols = 1;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                started.Add(temp.Date);
                            }
                            if (cabinet == 1 || table == 1 || stand == 1 || display == 1 || ucontrols == 1)
                            {
                                thereIsAStarted = true;
                                DateTime latestDate = started.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "UControls_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                ucontrols = 3;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                delayed.Add(temp.Date);
                            }
                            if (cabinet == 3 || table == 3 || stand == 3 || display == 3 || ucontrols == 3) //if one of them is delayed, display delayed in red
                            {
                                DateTime latestDate = delayed.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "Test_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 3;
                            }
                            j++;
                        }
                    }
                    dataGridView1.Rows.Add(row);

                    if (stripping == 0)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (stripping == 1)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (stripping == 2)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (stripping == 3)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (cleaning == 0)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (cleaning == 1)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (cleaning == 2)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (cleaning == 3)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (paint == 0)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (paint == 1)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (paint == 2)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (paint == 3)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 179, 186); //red

                    if (cabinet == 0 || table == 0 || stand == 0 || display == 0 || ucontrols == 0) //if all are expected, display expected
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    if (cabinet == 1 || table == 1 || stand == 1 || display == 1 || ucontrols == 1) //if one is started, display started
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    if (cabinet == 2 || table == 2 || stand == 2 || display == 2 || ucontrols == 2) //if one is finished, it is started
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    if (cabinet == 2 && table == 2 && stand == 2 && display == 2 && ucontrols == 2) //if all are finished, display finished
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    if (cabinet == 3 || table == 3 || stand == 3 || display == 3 || ucontrols == 3) //if one is delayed, display delayed
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                   

                    if (test == 0)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (test == 1)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (test == 2)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (test == 3)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (prepack == 0)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (prepack == 1)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (prepack == 2)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (prepack == 3)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (endpack == 0)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (endpack == 1)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (endpack == 2)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (endpack == 3)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (release == 0)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (release == 1)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (release == 2)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (release == 3)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void ImportMR(string fileName, int projectNr)
        {
            try
            {
                // get lines from the text file
                string[] lines = File.ReadAllLines($"../../{fileName}.txt");
                string[] values;
                List<DateTime> expected = new List<DateTime>();
                List<DateTime> started = new List<DateTime>();
                List<DateTime> finished = new List<DateTime>();
                List<DateTime> delayed = new List<DateTime>();
                for (int i = 0; i < lines.Length; i++)
                {
                    values = lines[i].ToString().Split(',', '=');
                    string[] row = new string[12];
                    int stripping = -1;
                    int cleaning = -1;
                    int paint = -1;
                    int backend = -1;
                    int magnet = -1;
                    int test = -1;
                    int prepack = -1;
                    int endpack = -1;
                    int release = -1;
                    bool thereIsAStarted = false;

                    for (int j = 0; j < values.Length; j++)
                    {
                        row[0] = values[0].Trim();
                        row[1] = values[1].Trim();
                        row[2] = values[2].Trim();
                        row[3] = values[3].Trim();
                        if (values[j] == "Stripping_e")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_s")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_f")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_d")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_e")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim();
                                paint = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim();
                                paint = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim(); paint = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim(); paint = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Backend_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                expected.Add(temp.Date);
                                backend = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Backend_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                started.Add(temp.Date);
                                backend = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Backend_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                finished.Add(temp.Date);
                                backend = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Backend_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                delayed.Add(temp.Date);
                                backend = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Magnet_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                magnet = 0;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                expected.Add(temp.Date);
                            }
                            if (backend == 0 || magnet == 0)
                            {
                                DateTime latestDate = expected.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "Magnet_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                magnet = 1;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                started.Add(temp.Date);
                            }
                            if (backend == 1 || magnet == 1)
                            {
                                thereIsAStarted = true;
                                DateTime latestDate = started.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "Magnet_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                magnet = 2;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                finished.Add(temp.Date);
                            }
                            if ((backend == 2 || magnet == 2) && thereIsAStarted == false) //if one is finished, display started
                            {
                                DateTime latestDate = finished.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            if (backend == 2 && magnet == 2) //only when all of them are finished, display finished
                            {
                                DateTime latestDate = finished.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "Magnet_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                magnet = 3;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                delayed.Add(temp.Date);
                            }
                            if (backend == 3 || magnet == 3) //if one of them is delayed, display delayed in red
                            {
                                DateTime latestDate = delayed.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "Test_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 3;
                            }
                            j++;
                        }
                    }
                    dataGridView1.Rows.Add(row);

                    if (stripping == 0)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (stripping == 1)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (stripping == 2)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (stripping == 3)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (cleaning == 0)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (cleaning == 1)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (cleaning == 2)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (cleaning == 3)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (paint == 0)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (paint == 1)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (paint == 2)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (paint == 3)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 179, 186); //red

                    if (backend == 0 || magnet == 0) //if all are expected, display expected
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    if (backend == 1 || magnet == 1) //if one is started, display started
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    if (backend == 2 || magnet == 2) //if one is finished, it is started
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    if (backend == 2 && magnet == 2) //if all are finished, display finished
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    if (backend == 3 || magnet == 3) //if one is delayed, display delayed
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 179, 186); //red

                    if (test == 0)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (test == 1)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (test == 2)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (test == 3)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (prepack == 0)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (prepack == 1)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (prepack == 2)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (prepack == 3)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (endpack == 0)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (endpack == 1)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (endpack == 2)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (endpack == 3)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (release == 0)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (release == 1)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (release == 2)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (release == 3)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void ImportCT(string fileName, int projectNr)
        {
            try
            {
                // get lines from the text file
                string[] lines = File.ReadAllLines($"../../{fileName}.txt");
                string[] values;
                List<DateTime> expected = new List<DateTime>();
                List<DateTime> started = new List<DateTime>();
                List<DateTime> finished = new List<DateTime>();
                List<DateTime> delayed = new List<DateTime>();
                for (int i = 0; i < lines.Length; i++)
                {
                    values = lines[i].ToString().Split(',', '=');
                    string[] row = new string[12];
                    int stripping = -1;
                    int cleaning = -1;
                    int paint = -1;
                    int patient = -1;
                    int support = -1;
                    int gantry = -1;
                    int test = -1;
                    int prepack = -1;
                    int endpack = -1;
                    int release = -1;
                    bool thereIsAStarted = false;

                    for (int j = 0; j < values.Length; j++)
                    {
                        row[0] = values[0].Trim();
                        row[1] = values[1].Trim();
                        row[2] = values[2].Trim();
                        row[3] = values[3].Trim();
                        if (values[j] == "Stripping_e")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_s")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_f")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_d")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_e")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim();
                                paint = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim();
                                paint = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim(); paint = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim(); paint = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Patient_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                expected.Add(temp.Date);
                                patient = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Patient_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                started.Add(temp.Date);
                                patient = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Patient_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                finished.Add(temp.Date);
                                patient = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Patient_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                delayed.Add(temp.Date);
                                patient = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Support_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                expected.Add(temp.Date);
                                support = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Support_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                started.Add(temp.Date);
                                support = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Support_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                finished.Add(temp.Date);
                                support = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Support_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                delayed.Add(temp.Date);
                                support = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Gantry_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                gantry = 0;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                expected.Add(temp.Date);
                            }
                            if (patient == 0 || support == 0 || gantry == 0)
                            {
                                DateTime latestDate = expected.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "Gantry_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                gantry = 1;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                started.Add(temp.Date);
                            }
                            if (patient == 1 || support == 1 || gantry == 1)
                            {
                                thereIsAStarted = true;
                                DateTime latestDate = started.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "Gantry_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                gantry = 2;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                finished.Add(temp.Date);
                            }
                            if ((patient == 2 || support == 2 || gantry == 2) && thereIsAStarted == false) //if one is finished, display started (with finished date)
                            {
                                DateTime latestDate = finished.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            if (patient == 2 && support == 2 && gantry == 2) //only when all of them are finished, display finished
                            {
                                DateTime latestDate = finished.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "Gantry_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                gantry = 3;
                                DateTime temp = DateTime.ParseExact(values[j + 1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                delayed.Add(temp.Date);
                            }
                            if (patient == 3 || support == 3 || gantry == 3) //if one of them is delayed, display delayed in red
                            {
                                DateTime latestDate = delayed.Min(r => r.Date);
                                row[7] = latestDate.ToString("dd/MM/yyyy");
                            }
                            j++;
                        }
                        else if (values[j] == "Test_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 3;
                            }
                            j++;
                        }
                    }
                    dataGridView1.Rows.Add(row);

                    if (stripping == 0)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (stripping == 1)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (stripping == 2)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (stripping == 3)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (cleaning == 0)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (cleaning == 1)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (cleaning == 2)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (cleaning == 3)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (paint == 0)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (paint == 1)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (paint == 2)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (paint == 3)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 179, 186); //red

                    if (patient == 0 || support == 0 || gantry == 0) //if all are expected, display expected
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    if (patient == 1 || support == 1 || gantry == 1) //if one is started, display started
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    if (patient == 2 || support == 2 || gantry == 2) //if one is finished, it is started
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    if (patient == 2 && support == 2 && gantry == 2) //if all are finished, display finished
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    if (patient == 3 || support == 3 || gantry == 3) //if one is delayed, display delayed
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 179, 186); //red

                    if (test == 0)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (test == 1)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (test == 2)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (test == 3)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (prepack == 0)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (prepack == 1)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (prepack == 2)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (prepack == 3)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (endpack == 0)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (endpack == 1)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (endpack == 2)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (endpack == 3)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (release == 0)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (release == 1)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (release == 2)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (release == 3)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void ImportSUR(string fileName, int projectNr)
        {
            try
            {
                // get lines from the text file
                string[] lines = File.ReadAllLines($"../../{fileName}.txt");
                string[] values;

                for (int i = 0; i < lines.Length; i++)
                {
                    values = lines[i].ToString().Split(',', '=');
                    string[] row = new string[12];
                    int stripping = -1;
                    int cleaning = -1;
                    int paint = -1;
                    int refurbishment = -1;
                    int test = -1;
                    int prepack = -1;
                    int endpack = -1;
                    int release = -1;

                    for (int j = 0; j < values.Length; j++)
                    {
                        row[0] = values[0].Trim();
                        row[1] = values[1].Trim();
                        row[2] = values[2].Trim();
                        row[3] = values[3].Trim();
                        if (values[j] == "Stripping_e")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_s")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_f")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Stripping_d")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[4] = values[j + 1].Trim();
                                stripping = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_e")
                        {
                            if (values[j + 1].Trim() != "")
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Cleaning_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[5] = values[j + 1].Trim();
                                cleaning = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim();
                                paint = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim();
                                paint = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim(); paint = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Paint_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[6] = values[j + 1].Trim(); paint = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Refurbishment_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[7] = values[j + 1].Trim(); refurbishment = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Refurbishment_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[7] = values[j + 1].Trim(); refurbishment = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Refurbishment_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[7] = values[j + 1].Trim(); refurbishment = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Refurbishment_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[7] = values[j + 1].Trim(); refurbishment = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Test_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[8] = values[j + 1].Trim(); test = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Prepack_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[9] = values[j + 1].Trim(); prepack = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Endpack_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[10] = values[j + 1].Trim(); endpack = 3;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_e")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 0;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_s")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 1;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_f")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 2;
                            }
                            j++;
                        }
                        else if (values[j] == "Release_d")
                        {
                            if (values[j + 1].Trim() != "") //check if there is a date entered
                            {
                                row[11] = values[j + 1].Trim(); release = 3;
                            }
                            j++;
                        }
                    }
                    dataGridView1.Rows.Add(row);

                    if (stripping == 0)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (stripping == 1)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (stripping == 2)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (stripping == 3)
                        dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (cleaning == 0)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (cleaning == 1)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (cleaning == 2)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (cleaning == 3)
                        dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (paint == 0)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (paint == 1)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (paint == 2)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (paint == 3)
                        dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (refurbishment == 0)
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (refurbishment == 1)
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (refurbishment == 2)
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (refurbishment == 3)
                        dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (test == 0)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (test == 1)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (test == 2)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (test == 3)
                        dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (prepack == 0)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (prepack == 1)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (prepack == 2)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (prepack == 3)
                        dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (endpack == 0)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (endpack == 1)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (endpack == 2)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (endpack == 3)
                        dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                    if (release == 0)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                    else if (release == 1)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                    else if (release == 2)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                    else if (release == 3)
                        dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnMR_Click(object sender, EventArgs e)
        {
            MR mr = new MR();
            mr.Show();
            this.Visible = false;
        }

        private void btnCT_Click(object sender, EventArgs e)
        {
            CT ct = new CT();
            ct.Show();
            this.Visible = false;
        }

        private void btnSUR_Click(object sender, EventArgs e)
        {
            SUR surgery = new SUR();
            surgery.Show();
            this.Visible = false;
        }

        private void btnAddNewRecord_Click(object sender, EventArgs e)
        {
            AddOrUpdateRecord igt = new AddOrUpdateRecord(0);
            igt.Show();
            this.Visible = false;
        }
    }
}
