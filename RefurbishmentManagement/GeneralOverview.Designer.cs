﻿namespace RefurbishmentManagement
{
    partial class GeneralOverview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.SalesNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modelity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Stripping = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cleaning = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Painting = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Refurbishment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Test = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prepack = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FinalPack = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReleaseForDelivery = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSUR = new System.Windows.Forms.Button();
            this.btnMR = new System.Windows.Forms.Button();
            this.btnCT = new System.Windows.Forms.Button();
            this.btnIGT = new System.Windows.Forms.Button();
            this.panel46 = new System.Windows.Forms.Panel();
            this.panel47 = new System.Windows.Forms.Panel();
            this.panel48 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel49 = new System.Windows.Forms.Panel();
            this.button28 = new System.Windows.Forms.Button();
            this.btnAddNewRecord = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel47.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SalesNr,
            this.ProjectName,
            this.Modelity,
            this.Type,
            this.Stripping,
            this.Cleaning,
            this.Painting,
            this.Refurbishment,
            this.Test,
            this.Prepack,
            this.FinalPack,
            this.ReleaseForDelivery});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(15, 60);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1516, 584);
            this.dataGridView1.TabIndex = 20;
            // 
            // SalesNr
            // 
            this.SalesNr.HeaderText = "Sales Nr";
            this.SalesNr.Name = "SalesNr";
            this.SalesNr.ReadOnly = true;
            // 
            // ProjectName
            // 
            this.ProjectName.HeaderText = "Project Name";
            this.ProjectName.Name = "ProjectName";
            this.ProjectName.ReadOnly = true;
            // 
            // Modelity
            // 
            this.Modelity.HeaderText = "Modelity";
            this.Modelity.Name = "Modelity";
            this.Modelity.ReadOnly = true;
            // 
            // Type
            // 
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // Stripping
            // 
            this.Stripping.HeaderText = "Stripping";
            this.Stripping.Name = "Stripping";
            this.Stripping.ReadOnly = true;
            // 
            // Cleaning
            // 
            this.Cleaning.HeaderText = "Cleaning";
            this.Cleaning.Name = "Cleaning";
            this.Cleaning.ReadOnly = true;
            // 
            // Painting
            // 
            this.Painting.HeaderText = "Paint";
            this.Painting.Name = "Painting";
            this.Painting.ReadOnly = true;
            // 
            // Refurbishment
            // 
            this.Refurbishment.HeaderText = "Refurbishment";
            this.Refurbishment.Name = "Refurbishment";
            this.Refurbishment.ReadOnly = true;
            // 
            // Test
            // 
            this.Test.HeaderText = "Test";
            this.Test.Name = "Test";
            this.Test.ReadOnly = true;
            // 
            // Prepack
            // 
            this.Prepack.HeaderText = "Pre-pack";
            this.Prepack.Name = "Prepack";
            this.Prepack.ReadOnly = true;
            // 
            // FinalPack
            // 
            this.FinalPack.HeaderText = "Final Pack";
            this.FinalPack.Name = "FinalPack";
            this.FinalPack.ReadOnly = true;
            // 
            // ReleaseForDelivery
            // 
            this.ReleaseForDelivery.HeaderText = "Release For Delivery";
            this.ReleaseForDelivery.Name = "ReleaseForDelivery";
            this.ReleaseForDelivery.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSUR);
            this.groupBox1.Controls.Add(this.btnMR);
            this.groupBox1.Controls.Add(this.btnCT);
            this.groupBox1.Controls.Add(this.btnIGT);
            this.groupBox1.Controls.Add(this.panel46);
            this.groupBox1.Controls.Add(this.btnAddNewRecord);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(15, 644);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(1516, 95);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            // 
            // btnSUR
            // 
            this.btnSUR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSUR.FlatAppearance.BorderSize = 0;
            this.btnSUR.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSUR.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSUR.Location = new System.Drawing.Point(211, 18);
            this.btnSUR.Name = "btnSUR";
            this.btnSUR.Size = new System.Drawing.Size(59, 27);
            this.btnSUR.TabIndex = 31;
            this.btnSUR.Text = "SUR";
            this.btnSUR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSUR.UseVisualStyleBackColor = true;
            this.btnSUR.Click += new System.EventHandler(this.btnSUR_Click);
            // 
            // btnMR
            // 
            this.btnMR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnMR.FlatAppearance.BorderSize = 0;
            this.btnMR.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMR.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnMR.Location = new System.Drawing.Point(82, 18);
            this.btnMR.Name = "btnMR";
            this.btnMR.Size = new System.Drawing.Size(59, 27);
            this.btnMR.TabIndex = 30;
            this.btnMR.Text = "MR\r\n";
            this.btnMR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMR.UseVisualStyleBackColor = true;
            this.btnMR.Click += new System.EventHandler(this.btnMR_Click);
            // 
            // btnCT
            // 
            this.btnCT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCT.FlatAppearance.BorderSize = 0;
            this.btnCT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCT.Location = new System.Drawing.Point(146, 18);
            this.btnCT.Name = "btnCT";
            this.btnCT.Size = new System.Drawing.Size(59, 27);
            this.btnCT.TabIndex = 29;
            this.btnCT.Text = "CT";
            this.btnCT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCT.UseVisualStyleBackColor = true;
            this.btnCT.Click += new System.EventHandler(this.btnCT_Click);
            // 
            // btnIGT
            // 
            this.btnIGT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnIGT.FlatAppearance.BorderSize = 0;
            this.btnIGT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnIGT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnIGT.Location = new System.Drawing.Point(19, 18);
            this.btnIGT.Name = "btnIGT";
            this.btnIGT.Size = new System.Drawing.Size(59, 27);
            this.btnIGT.TabIndex = 28;
            this.btnIGT.Text = "IGT";
            this.btnIGT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIGT.UseVisualStyleBackColor = true;
            this.btnIGT.Click += new System.EventHandler(this.btnIGT_Click);
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.Black;
            this.panel46.Controls.Add(this.panel47);
            this.panel46.Controls.Add(this.button1);
            this.panel46.Controls.Add(this.panel49);
            this.panel46.Controls.Add(this.button28);
            this.panel46.Location = new System.Drawing.Point(1229, 16);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(240, 1);
            this.panel46.TabIndex = 26;
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.Color.Black;
            this.panel47.Controls.Add(this.panel48);
            this.panel47.Controls.Add(this.button8);
            this.panel47.Location = new System.Drawing.Point(0, 0);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(225, 1);
            this.panel47.TabIndex = 6;
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.Black;
            this.panel48.Location = new System.Drawing.Point(-86, -11);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(225, 1);
            this.panel48.TabIndex = 4;
            // 
            // button8
            // 
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button8.Location = new System.Drawing.Point(-68, -31);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(378, 43);
            this.button8.TabIndex = 3;
            this.button8.Text = "Register A Request";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button1.Location = new System.Drawing.Point(18, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(378, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Register A Request";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.Black;
            this.panel49.Location = new System.Drawing.Point(-86, -11);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(225, 1);
            this.panel49.TabIndex = 4;
            // 
            // button28
            // 
            this.button28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button28.FlatAppearance.BorderSize = 0;
            this.button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button28.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button28.Location = new System.Drawing.Point(-68, -11);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(378, 23);
            this.button28.TabIndex = 3;
            this.button28.Text = "Register A Request";
            this.button28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button28.UseVisualStyleBackColor = true;
            // 
            // btnAddNewRecord
            // 
            this.btnAddNewRecord.BackgroundImage = global::RefurbishmentManagement.Properties.Resources.arrowblack;
            this.btnAddNewRecord.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAddNewRecord.FlatAppearance.BorderSize = 0;
            this.btnAddNewRecord.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddNewRecord.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAddNewRecord.Location = new System.Drawing.Point(1227, 18);
            this.btnAddNewRecord.Name = "btnAddNewRecord";
            this.btnAddNewRecord.Size = new System.Drawing.Size(446, 27);
            this.btnAddNewRecord.TabIndex = 27;
            this.btnAddNewRecord.Text = "Add/Remove a Record";
            this.btnAddNewRecord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddNewRecord.UseVisualStyleBackColor = true;
            this.btnAddNewRecord.Click += new System.EventHandler(this.btnAddNewRecord_Click);
            // 
            // GeneralOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1546, 755);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "GeneralOverview";
            this.Padding = new System.Windows.Forms.Padding(15, 60, 15, 16);
            this.Text = "General Overview";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GeneralOverview_FormClosed);
            this.Load += new System.EventHandler(this.GeneralOverview_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modelity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Stripping;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cleaning;
        private System.Windows.Forms.DataGridViewTextBoxColumn Painting;
        private System.Windows.Forms.DataGridViewTextBoxColumn Refurbishment;
        private System.Windows.Forms.DataGridViewTextBoxColumn Test;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prepack;
        private System.Windows.Forms.DataGridViewTextBoxColumn FinalPack;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReleaseForDelivery;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button btnAddNewRecord;
        private System.Windows.Forms.Button btnSUR;
        private System.Windows.Forms.Button btnMR;
        private System.Windows.Forms.Button btnCT;
        private System.Windows.Forms.Button btnIGT;
    }
}