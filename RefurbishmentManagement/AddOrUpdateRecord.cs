﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RefurbishmentManagement
{
    public partial class AddOrUpdateRecord : MetroFramework.Forms.MetroForm
    {
        int toTurnBack;

        public AddOrUpdateRecord(int window)
        {
            InitializeComponent();
            toTurnBack = window;
        }
        
        private void btnTurnBack_Click(object sender, EventArgs e)
        {
            if (toTurnBack == 0)
            {
                GeneralOverview g = new GeneralOverview();
                g.Show();
            }
            else if (toTurnBack == 1)
            {
                IGT g = new IGT();
                g.Show();
            }
            else if (toTurnBack == 2)
            {
                MR g = new MR();
                g.Show();
            }
            else if (toTurnBack == 3)
            {
                CT g = new CT();
                g.Show();
            }
            else if (toTurnBack == 4)
            {
                SUR g = new SUR();
                g.Show();
            }
            this.Visible = false;
        }

        private void AddOrUpdateRecord_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            try
            {
                string enteredName = tbSalesNr.Text;
                string fileName = $"../../{enteredName}.txt"; //create a txt file with the sales nr
                string projectName;
                string type;
                string modelity = "";
                string salesNr = "";
                string[] lines = new string[300];
                // Check if file already exists. If yes, delete it.     
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
                if (string.Compare(cbModelity.SelectedItem.ToString(), "IGT") == 0)
                {
                    lines = File.ReadAllLines(@"..\..\igt_template.txt"); modelity = "IGT,";
                }
                else if (string.Compare(cbModelity.SelectedItem.ToString(), "MR") == 0)
                {
                    lines = File.ReadAllLines(@"..\..\mr_template.txt"); modelity = "MR,";
                }
                else if (string.Compare(cbModelity.SelectedItem.ToString(), "CT") == 0)
                {
                    lines = File.ReadAllLines(@"..\..\ct_template.txt"); modelity = "CT,";
                }
                else if (string.Compare(cbModelity.SelectedItem.ToString(), "SUR") == 0)
                {
                    lines = File.ReadAllLines(@"..\..\sur_template.txt"); modelity = "SUR,";
                }
                // Create a new file     
                using (FileStream fs = File.Create(fileName))
                {
                    salesNr = enteredName + ","; //unique key
                    projectName = tbProjectName.Text + ","; //city name
                    type = tbType.Text + ","; // 4th place

                    // Add some text to file    
                    Byte[] s = new UTF8Encoding(true).GetBytes(salesNr);
                    fs.Write(s, 0, s.Length);
                    byte[] n = new UTF8Encoding(true).GetBytes(projectName);
                    fs.Write(n, 0, n.Length);
                    byte[] m = new UTF8Encoding(true).GetBytes(modelity);
                    fs.Write(m, 0, m.Length);
                    byte[] t = new UTF8Encoding(true).GetBytes(type);
                    fs.Write(t, 0, t.Length);

                    foreach (string line in lines)
                    {
                        byte[] b = new UTF8Encoding(true).GetBytes(line);
                        fs.Write(b, 0, b.Length);
                    }
                }
                File.AppendAllText("../../AllRefurbishment.txt", modelity + salesNr + Environment.NewLine);

                // Open the stream and read it back.    
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        MessageBox.Show(s);
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        private void AddOrUpdateRecord_Load(object sender, EventArgs e)
        {
            //Select the modelity automatically if you are coming from one of the modelity interfaces
            if (toTurnBack == 0)
            {
            }
            else if (toTurnBack == 1)
            {
                cbModelity.SelectedIndex = 0;
            }
            else if (toTurnBack == 2)
            {
                cbModelity.SelectedIndex = 2;
            }
            else if (toTurnBack == 3)
            {
                cbModelity.SelectedIndex = 1;
            }
            else if (toTurnBack == 4)
            {
                cbModelity.SelectedIndex = 3;
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                string enteredName = tbRemoveSalesNr.Text;
                string tempFile = Path.GetTempFileName();

                using (var sr = new StreamReader(@"..\..\AllRefurbishment.txt"))
                using (var sw = new StreamWriter(tempFile))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (!line.Contains(enteredName))
                            sw.WriteLine(line);
                    }
                }
                File.Delete(@"..\..\AllRefurbishment.txt");
                File.Move(tempFile, @"..\..\AllRefurbishment.txt");

            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }
    }
}
