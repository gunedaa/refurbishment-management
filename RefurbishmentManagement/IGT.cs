﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace RefurbishmentManagement
{
    public partial class IGT : MetroFramework.Forms.MetroForm
    {
        GeneralOverview overview;
        DateTimePicker dateTimePicker1 = new DateTimePicker();
        ComboBox comboBox = new ComboBox();
        Rectangle cellRect;
        DataTable dtable = new DataTable();
        int clickedRow = -1;
        int clickedColumn = -1;

        public IGT()
        {
            InitializeComponent();
          
            dataGridView1.Controls.Add(dateTimePicker1);
            dataGridView1.Controls.Add(comboBox);
            
            comboBox.Items.Add("Expected");
            comboBox.Items.Add("Started");
            comboBox.Items.Add("Finished");
            comboBox.Items.Add("Delayed");
            comboBox.Visible = false;
            dateTimePicker1.Visible = false;
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            dateTimePicker1.TextChanged += new EventHandler(dtp_TextChange);
            comboBox.SelectedValueChanged += new EventHandler(cmb_TextChange);
            
        }
        private void dtp_TextChange(object sender, EventArgs e)
        {
            dataGridView1.CurrentCell.Value = dateTimePicker1.Text.ToString();
        }

        private void cmb_TextChange(object sender, EventArgs e)
        {
            if(comboBox.SelectedIndex == 0)
            {
                dataGridView1.CurrentCell.Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                //here write to the text file
                ExportIGTToTextFile(0);
            }
            if (comboBox.SelectedIndex == 1)
            {
                dataGridView1.CurrentCell.Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                ExportIGTToTextFile(1);
            }
            if (comboBox.SelectedIndex == 2)
            {
                dataGridView1.CurrentCell.Style.BackColor = Color.FromArgb(186, 255, 201); //green
                ExportIGTToTextFile(2);
            }
            if (comboBox.SelectedIndex == 3)
            {
                dataGridView1.CurrentCell.Style.BackColor = Color.FromArgb(255, 179, 186); //red
                ExportIGTToTextFile(3);
            }
            comboBox.Visible = false;
        }

        private void btnTurnBack_Click(object sender, EventArgs e)
        {
            overview = new GeneralOverview();
            overview.Show();
            this.Visible = false;
        }

        private void IGT_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        
        private void btnAddNewRecord_Click(object sender, EventArgs e)
        {
            AddOrUpdateRecord igt = new AddOrUpdateRecord(1);
            igt.Show();
            this.Visible = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1) return;
            if (dataGridView1.Columns[e.ColumnIndex].Name.CompareTo("SalesNr") != 0 && dataGridView1.Columns[e.ColumnIndex].Name.CompareTo("PName") != 0  &&
                 dataGridView1.Columns[e.ColumnIndex].Name.CompareTo("Type") != 0 && e.RowIndex > -1)
            {
                clickedRow = e.RowIndex;
                clickedColumn = e.ColumnIndex;
                cellRect = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                dateTimePicker1.Size = new Size(cellRect.Width, cellRect.Height);
                dateTimePicker1.Location = new Point(cellRect.X, cellRect.Y);
                dateTimePicker1.Visible = true;
            }
        }
      
        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            dateTimePicker1.Visible = false;
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            dateTimePicker1.Visible = false;
        }

        public void ExportIGTToTextFile(int selectedDate)
        {
            try
            {
                string filename = dataGridView1.Rows[clickedRow].Cells[0].Value.ToString();
                var text = new StringBuilder();

                foreach (string s in File.ReadAllLines($"../../{filename}.txt"))
                {
                    if (clickedColumn == 3 && selectedDate == 0)
                    {
                        var rx = new Regex(@"Stripping_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Stripping_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Stripping_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Stripping_s=");
                        var rx3 = new Regex(@"Stripping_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Stripping_f=");
                        var rx4 = new Regex(@"Stripping_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Stripping_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 3 && selectedDate == 1)
                    {
                        var rx = new Regex(@"Stripping_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Stripping_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Stripping_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Stripping_e=");
                        var rx3 = new Regex(@"Stripping_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Stripping_f=");
                        var rx4 = new Regex(@"Stripping_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Stripping_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 3 && selectedDate == 2)
                    {
                        var rx = new Regex(@"Stripping_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Stripping_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Stripping_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Stripping_e=");
                        var rx3 = new Regex(@"Stripping_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Stripping_s=");
                        var rx4 = new Regex(@"Stripping_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Stripping_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 3 && selectedDate == 3)
                    {
                        var rx = new Regex(@"Stripping_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Stripping_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Stripping_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Stripping_e=");
                        var rx3 = new Regex(@"Stripping_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Stripping_s=");
                        var rx4 = new Regex(@"Stripping_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Stripping_f=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 4 && selectedDate == 0)
                    {
                        var rx = new Regex(@"Cleaning_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Cleaning_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Cleaning_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Cleaning_s=");
                        var rx3 = new Regex(@"Cleaning_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Cleaning_f=");
                        var rx4 = new Regex(@"Cleaning_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Cleaning_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 4 && selectedDate == 1)
                    {
                        var rx = new Regex(@"Cleaning_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Cleaning_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Cleaning_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Cleaning_e=");
                        var rx3 = new Regex(@"Cleaning_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Cleaning_f=");
                        var rx4 = new Regex(@"Cleaning_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Cleaning_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 4 && selectedDate == 2)
                    {
                        var rx = new Regex(@"Cleaning_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Cleaning_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Cleaning_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Cleaning_e=");
                        var rx3 = new Regex(@"Cleaning_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Cleaning_s=");
                        var rx4 = new Regex(@"Cleaning_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Cleaning_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 4 && selectedDate == 3)
                    {
                        var rx = new Regex(@"Cleaning_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Cleaning_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Cleaning_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Cleaning_e=");
                        var rx3 = new Regex(@"Cleaning_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Cleaning_s=");
                        var rx4 = new Regex(@"Cleaning_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Cleaning_f=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 5 && selectedDate == 0)
                    {
                        var rx = new Regex(@"Paint_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Paint_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Paint_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Paint_s=");
                        var rx3 = new Regex(@"Paint_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Paint_f=");
                        var rx4 = new Regex(@"Paint_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Paint_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 5 && selectedDate == 1)
                    {
                        var rx = new Regex(@"Paint_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Paint_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Paint_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Paint_e=");
                        var rx3 = new Regex(@"Paint_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Paint_f=");
                        var rx4 = new Regex(@"Paint_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Paint_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 5 && selectedDate == 2)
                    {
                        var rx = new Regex(@"Paint_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Paint_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Paint_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Paint_e=");
                        var rx3 = new Regex(@"Paint_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Paint_s=");
                        var rx4 = new Regex(@"Paint_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Paint_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 5 && selectedDate == 3)
                    {
                        var rx = new Regex(@"Paint_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Paint_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Paint_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Paint_e=");
                        var rx3 = new Regex(@"Paint_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Paint_s=");
                        var rx4 = new Regex(@"Paint_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Paint_f=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 6 && selectedDate == 0)
                    {
                        var rx = new Regex(@"Cabinet_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Cabinet_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Cabinet_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Cabinet_s=");
                        var rx3 = new Regex(@"Cabinet_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Cabinet_f=");
                        var rx4 = new Regex(@"Cabinet_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Cabinet_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 6 && selectedDate == 1)
                    {
                        var rx = new Regex(@"Cabinet_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Cabinet_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Cabinet_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Cabinet_e=");
                        var rx3 = new Regex(@"Cabinet_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Cabinet_f=");
                        var rx4 = new Regex(@"Cabinet_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Cabinet_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 6 && selectedDate == 2)
                    {
                        var rx = new Regex(@"Cabinet_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Cabinet_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Cabinet_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Cabinet_e=");
                        var rx3 = new Regex(@"Cabinet_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Cabinet_s=");
                        var rx4 = new Regex(@"Cabinet_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Cabinet_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 6 && selectedDate == 3)
                    {
                        var rx = new Regex(@"Cabinet_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Cabinet_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Cabinet_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Cabinet_e=");
                        var rx3 = new Regex(@"Cabinet_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Cabinet_s=");
                        var rx4 = new Regex(@"Cabinet_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Cabinet_f=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 7 && selectedDate == 0)
                    {
                        var rx = new Regex(@"Table_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Table_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Table_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Table_s=");
                        var rx3 = new Regex(@"Table_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Table_f=");
                        var rx4 = new Regex(@"Table_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Table_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 7 && selectedDate == 1)
                    {
                        var rx = new Regex(@"Table_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Table_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Table_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Table_e=");
                        var rx3 = new Regex(@"Table_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Table_f=");
                        var rx4 = new Regex(@"Table_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Table_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 7 && selectedDate == 2)
                    {
                        var rx = new Regex(@"Table_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Table_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Table_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Table_e=");
                        var rx3 = new Regex(@"Table_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Table_s=");
                        var rx4 = new Regex(@"Table_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Table_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 7 && selectedDate == 3)
                    {
                        var rx = new Regex(@"Table_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Table_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Table_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Table_e=");
                        var rx3 = new Regex(@"Table_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Table_s=");
                        var rx4 = new Regex(@"Table_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Table_f=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 8 && selectedDate == 0)
                    {
                        var rx = new Regex(@"Stand_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Stand_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Stand_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Stand_s=");
                        var rx3 = new Regex(@"Stand_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Stand_f=");
                        var rx4 = new Regex(@"Stand_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Stand_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 8 && selectedDate == 1)
                    {
                        var rx = new Regex(@"Stand_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Stand_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Stand_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Stand_e=");
                        var rx3 = new Regex(@"Stand_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Stand_f=");
                        var rx4 = new Regex(@"Stand_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Stand_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 8 && selectedDate == 2)
                    {
                        var rx = new Regex(@"Stand_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Stand_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Stand_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Stand_e=");
                        var rx3 = new Regex(@"Stand_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Stand_s=");
                        var rx4 = new Regex(@"Stand_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Stand_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 8 && selectedDate == 3)
                    {
                        var rx = new Regex(@"Stand_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Stand_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Stand_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Stand_e=");
                        var rx3 = new Regex(@"Stand_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Stand_s=");
                        var rx4 = new Regex(@"Stand_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Stand_f=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 9 && selectedDate == 0)
                    {
                        var rx = new Regex(@"Display_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Display_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Display_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Display_s=");
                        var rx3 = new Regex(@"Display_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Display_f=");
                        var rx4 = new Regex(@"Display_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Display_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 9 && selectedDate == 1)
                    {
                        var rx = new Regex(@"Display_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Display_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Display_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Display_e=");
                        var rx3 = new Regex(@"Display_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Display_f=");
                        var rx4 = new Regex(@"Display_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Display_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 9 && selectedDate == 2)
                    {
                        var rx = new Regex(@"Display_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Display_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Display_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Display_e=");
                        var rx3 = new Regex(@"Display_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Display_s=");
                        var rx4 = new Regex(@"Display_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Display_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 9 && selectedDate == 3)
                    {
                        var rx = new Regex(@"Display_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Display_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Display_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Display_e=");
                        var rx3 = new Regex(@"Display_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Display_s=");
                        var rx4 = new Regex(@"Display_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Display_f=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 10 && selectedDate == 0)
                    {
                        var rx = new Regex(@"UControls_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"UControls_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"UControls_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"UControls_s=");
                        var rx3 = new Regex(@"UControls_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"UControls_f=");
                        var rx4 = new Regex(@"UControls_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"UControls_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 10 && selectedDate == 1)
                    {
                        var rx = new Regex(@"UControls_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"UControls_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"UControls_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"UControls_e=");
                        var rx3 = new Regex(@"UControls_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"UControls_f=");
                        var rx4 = new Regex(@"UControls_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"UControls_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 10 && selectedDate == 2)
                    {
                        var rx = new Regex(@"UControls_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"UControls_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"UControls_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"UControls_e=");
                        var rx3 = new Regex(@"UControls_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"UControls_s=");
                        var rx4 = new Regex(@"UControls_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"UControls_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 10 && selectedDate == 3)
                    {
                        var rx = new Regex(@"UControls_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"UControls_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"UControls_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"UControls_e=");
                        var rx3 = new Regex(@"UControls_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"UControls_s=");
                        var rx4 = new Regex(@"UControls_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"UControls_f=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 11 && selectedDate == 0)
                    {
                        var rx = new Regex(@"Test_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Test_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Test_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Test_s=");
                        var rx3 = new Regex(@"Test_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Test_f=");
                        var rx4 = new Regex(@"Test_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Test_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 11 && selectedDate == 1)
                    {
                        var rx = new Regex(@"Test_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Test_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Test_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Test_e=");
                        var rx3 = new Regex(@"Test_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Test_f=");
                        var rx4 = new Regex(@"Test_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Test_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 11 && selectedDate == 2)
                    {
                        var rx = new Regex(@"Test_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Test_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Test_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Test_e=");
                        var rx3 = new Regex(@"Test_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Test_s=");
                        var rx4 = new Regex(@"Test_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Test_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 11 && selectedDate == 3)
                    {
                        var rx = new Regex(@"Test_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Test_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Test_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Test_e=");
                        var rx3 = new Regex(@"Test_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Test_s=");
                        var rx4 = new Regex(@"Test_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Test_f=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 12 && selectedDate == 0)
                    {
                        var rx = new Regex(@"Prepack_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Prepack_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Prepack_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Prepack_s=");
                        var rx3 = new Regex(@"Prepack_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Prepack_f=");
                        var rx4 = new Regex(@"Prepack_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Prepack_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 12 && selectedDate == 1)
                    {
                        var rx = new Regex(@"Prepack_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Prepack_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Prepack_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Prepack_e=");
                        var rx3 = new Regex(@"Prepack_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Prepack_f=");
                        var rx4 = new Regex(@"Prepack_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Prepack_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 12 && selectedDate == 2)
                    {
                        var rx = new Regex(@"Prepack_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Prepack_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Prepack_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Prepack_e=");
                        var rx3 = new Regex(@"Prepack_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Prepack_s=");
                        var rx4 = new Regex(@"Prepack_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Prepack_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 12 && selectedDate == 3)
                    {
                        var rx = new Regex(@"Prepack_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Prepack_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Prepack_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Prepack_e=");
                        var rx3 = new Regex(@"Prepack_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Prepack_s=");
                        var rx4 = new Regex(@"Prepack_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Prepack_f=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 13 && selectedDate == 0)
                    {
                        var rx = new Regex(@"Endpack_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Endpack_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Endpack_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Endpack_s=");
                        var rx3 = new Regex(@"Endpack_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Endpack_f=");
                        var rx4 = new Regex(@"Endpack_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Endpack_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 13 && selectedDate == 1)
                    {
                        var rx = new Regex(@"Endpack_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Endpack_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Endpack_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Endpack_e=");
                        var rx3 = new Regex(@"Endpack_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Endpack_f=");
                        var rx4 = new Regex(@"Endpack_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Endpack_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 13 && selectedDate == 2)
                    {
                        var rx = new Regex(@"Endpack_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Endpack_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Endpack_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Endpack_e=");
                        var rx3 = new Regex(@"Endpack_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Endpack_s=");
                        var rx4 = new Regex(@"Endpack_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Endpack_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 13 && selectedDate == 3)
                    {
                        var rx = new Regex(@"Endpack_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Endpack_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Endpack_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Endpack_e=");
                        var rx3 = new Regex(@"Endpack_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Endpack_s=");
                        var rx4 = new Regex(@"Endpack_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Endpack_f=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 14 && selectedDate == 0)
                    {
                        var rx = new Regex(@"Release_e=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Release_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Release_s=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Release_s=");
                        var rx3 = new Regex(@"Release_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Release_f=");
                        var rx4 = new Regex(@"Release_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Release_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 14 && selectedDate == 1)
                    {
                        var rx = new Regex(@"Release_s=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Release_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Release_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Release_e=");
                        var rx3 = new Regex(@"Release_f=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Release_f=");
                        var rx4 = new Regex(@"Release_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Release_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 14 && selectedDate == 2)
                    {
                        var rx = new Regex(@"Release_f=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Release_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Release_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Release_e=");
                        var rx3 = new Regex(@"Release_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Release_s=");
                        var rx4 = new Regex(@"Release_d=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Release_d=");
                        text.AppendLine(newText);
                    }
                    else if (clickedColumn == 14 && selectedDate == 3)
                    {
                        var rx = new Regex(@"Release_d=(\d+/\d+/\d+)?");
                        string newText = rx.Replace(s, $"Release_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                        var rx2 = new Regex(@"Release_e=(\d+/\d+/\d+)?");
                        newText = rx2.Replace(newText, $"Release_e=");
                        var rx3 = new Regex(@"Release_s=(\d+/\d+/\d+)?");
                        newText = rx3.Replace(newText, $"Release_s=");
                        var rx4 = new Regex(@"Release_f=(\d+/\d+/\d+)?");
                        newText = rx4.Replace(newText, $"Release_f=");
                        text.AppendLine(newText);
                    }
                }

                using (var file = new StreamWriter(File.Create($"../../{filename}.txt")))
                {
                    file.Write(text.ToString());
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            dateTimePicker1.Visible = false;
            cellRect = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
            comboBox.Size = new Size(cellRect.Width, cellRect.Height);
            comboBox.Location = new Point(cellRect.X, cellRect.Y);
            comboBox.Visible = true;
        }

        private void dataGridView1_CellStyleChanged(object sender, DataGridViewCellEventArgs e)
        {
            comboBox.Visible = false;
        }

        private void IGT_Load(object sender, EventArgs e)
        {
            dtable.Columns.Add("SalesNr", typeof(int));
            dtable.Columns.Add("PName", typeof(string));
            dtable.Columns.Add("Type", typeof(string));
            dtable.Columns.Add("Stripping", typeof(string));
            dtable.Columns.Add("Cleaning", typeof(string));
            dtable.Columns.Add("Paint", typeof(string));
            dtable.Columns.Add("Cabinet", typeof(string));
            dtable.Columns.Add("Table", typeof(string));
            dtable.Columns.Add("Stand", typeof(string));
            dtable.Columns.Add("Display", typeof(string));
            dtable.Columns.Add("User Controls", typeof(string));
            dtable.Columns.Add("Test", typeof(string));
            dtable.Columns.Add("Pre-pack", typeof(string));
            dtable.Columns.Add("End-pack", typeof(string));
            dtable.Columns.Add("Release for Delivery", typeof(string));
            dataGridView1.DataSource = dtable;
            List<string> projects = GetIGTProjects();
            if (projects != null)
            {
                int nr = 0;
                foreach(string s in projects)
                {
                    ImportIGT(s, nr);
                    nr++;
                }
            }
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            WindowState = FormWindowState.Maximized;
            // Import("567", 0);
            // Import("678", 0);
        }
        private List<string> GetIGTProjects()
        {
            // get lines from the text file
            string[] lines = File.ReadAllLines(@"..\..\AllRefurbishment.txt");
            string[] values;
            string[] row = null;
            List<string> allProjects = new List<string>();

            for (int i = 0; i < lines.Length; i++)
            {
                values = lines[i].ToString().Split(',');
                row = new string[values.Length];

                for (int j = 0; j < values.Length; j++)
                {
                    if(values[j] == "IGT")
                    {
                        row[j] = values[j+1].Trim();
                        allProjects.Add(row[j]);
                    }
                }
            }
            return allProjects;
        }

        public void ImportIGT(string fileName, int projectNr)
        {
            // get lines from the text file
            string[] lines = File.ReadAllLines($"../../{fileName}.txt");
            string[] values;

            for (int i = 0; i < lines.Length; i++)
            {
                values = lines[i].ToString().Split(',', '=');
                string[] row = new string[15];
                int stripping = -1;
                int cleaning = -1;
                int paint = -1;
                int cabinet = -1;
                int table = -1;
                int stand = -1;
                int display = -1;
                int ucontrols = -1;
                int test = -1;
                int prepack = -1;
                int endpack = -1;
                int release = -1;

                for (int j = 0; j < values.Length; j++)
                {
                    row[0] = values[0].Trim();
                    row[1] = values[1].Trim();
                    row[2] = values[3].Trim();
                    if (values[j] == "Stripping_e")
                    {
                        if (values[j + 1].Trim() != "")
                        {
                            row[3] = values[j + 1].Trim();
                            stripping = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Stripping_s")
                    {
                        if (values[j + 1].Trim() != "")
                        {
                            row[3] = values[j + 1].Trim();
                            stripping = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Stripping_f")
                    {
                        if (values[j + 1].Trim() != "")
                        {
                            row[3] = values[j + 1].Trim();
                            stripping = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Stripping_d")
                    {
                        if (values[j + 1].Trim() != "")
                        {
                            row[3] = values[j + 1].Trim();
                            stripping = 3;
                        }
                        j++;
                    }
                    else if(values[j] == "Cleaning_e")
                    {
                        if (values[j + 1].Trim() != "")
                        {
                            row[4] = values[j + 1].Trim();
                            cleaning = 0;
                        }
                        j++;
                    }
                    else if(values[j] == "Cleaning_s")
                    {
                        if(values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[4] = values[j + 1].Trim();
                            cleaning = 1;
                        }
                        j++;
                    }
                    else if(values[j] == "Cleaning_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[4] = values[j + 1].Trim();
                            cleaning = 2;
                        }
                        j++;
                    }
                    else if(values[j] == "Cleaning_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[4] = values[j + 1].Trim();
                            cleaning = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Paint_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[5] = values[j + 1].Trim();
                            paint = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Paint_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[5] = values[j + 1].Trim();
                            paint = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Paint_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[5] = values[j + 1].Trim(); paint = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Paint_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[5] = values[j + 1].Trim(); paint = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Cabinet_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[6] = values[j + 1].Trim(); cabinet = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Cabinet_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[6] = values[j + 1].Trim(); cabinet = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Cabinet_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[6] = values[j + 1].Trim(); cabinet = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Cabinet_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[6] = values[j + 1].Trim(); cabinet = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Table_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[7] = values[j + 1].Trim();
                            table = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Table_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[7] = values[j + 1].Trim(); table = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Table_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[7] = values[j + 1].Trim(); table = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Table_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[7] = values[j + 1].Trim(); table = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Stand_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[8] = values[j + 1].Trim(); stand = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Stand_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[8] = values[j + 1].Trim(); stand = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Stand_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[8] = values[j + 1].Trim(); stand = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Stand_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[8] = values[j + 1].Trim(); stand = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Display_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[9] = values[j + 1].Trim(); display = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Display_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[9] = values[j + 1].Trim(); display = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Display_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[9] = values[j + 1].Trim(); display = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Display_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[9] = values[j + 1].Trim(); display = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "UControls_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[10] = values[j + 1].Trim(); ucontrols = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "UControls_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[10] = values[j + 1].Trim(); ucontrols = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "UControls_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[10] = values[j + 1].Trim(); ucontrols = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "UControls_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[10] = values[j + 1].Trim(); ucontrols = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Test_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[11] = values[j + 1].Trim(); test = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Test_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[11] = values[j + 1].Trim(); test = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Test_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[11] = values[j + 1].Trim(); test = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Test_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[11] = values[j + 1].Trim(); test = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Prepack_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[12] = values[j + 1].Trim(); prepack = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Prepack_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[12] = values[j + 1].Trim(); prepack = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Prepack_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[12] = values[j + 1].Trim(); prepack = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Prepack_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[12] = values[j + 1].Trim(); prepack = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Endpack_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[13] = values[j + 1].Trim(); endpack = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Endpack_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[13] = values[j + 1].Trim(); endpack = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Endpack_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[13] = values[j + 1].Trim(); endpack = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Endpack_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[13] = values[j + 1].Trim(); endpack = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Release_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[14] = values[j + 1].Trim(); release = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Release_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[14] = values[j + 1].Trim(); release = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Release_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[14] = values[j + 1].Trim(); release = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Release_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[14] = values[j + 1].Trim(); release = 3;
                        }
                        j++;
                    }
                }
                dtable.Rows.Add(row);

                if(stripping == 0)
                    dataGridView1.Rows[projectNr].Cells[3].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if(stripping == 1)
                    dataGridView1.Rows[projectNr].Cells[3].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (stripping == 2)
                    dataGridView1.Rows[projectNr].Cells[3].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (stripping == 3)
                    dataGridView1.Rows[projectNr].Cells[3].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (cleaning == 0)
                    dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (cleaning == 1)
                    dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (cleaning == 2)
                    dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (cleaning == 3)
                    dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (paint == 0)
                    dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (paint == 1)
                    dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (paint == 2)
                    dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (paint == 3)
                    dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (cabinet == 0)
                    dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (cabinet == 1)
                    dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (cabinet == 2)
                    dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (cabinet == 3)
                    dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (table == 0)
                    dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (table == 1)
                    dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (table == 2)
                    dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (table == 3)
                    dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (stand == 0)
                    dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (stand == 1)
                    dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (stand == 2)
                    dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (stand == 3)
                    dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (display == 0)
                    dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (display == 1)
                    dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (display == 2)
                    dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (display == 3)
                    dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (ucontrols == 0)
                    dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (ucontrols == 1)
                    dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (ucontrols == 2)
                    dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (ucontrols == 3)
                    dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (test == 0)
                    dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (test == 1)
                    dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (test == 2)
                    dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (test == 3)
                    dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (prepack == 0)
                    dataGridView1.Rows[projectNr].Cells[12].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (prepack == 1)
                    dataGridView1.Rows[projectNr].Cells[12].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (prepack == 2)
                    dataGridView1.Rows[projectNr].Cells[12].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (prepack == 3)
                    dataGridView1.Rows[projectNr].Cells[12].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (endpack == 0)
                    dataGridView1.Rows[projectNr].Cells[13].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (endpack == 1)
                    dataGridView1.Rows[projectNr].Cells[13].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (endpack == 2)
                    dataGridView1.Rows[projectNr].Cells[13].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (endpack == 3)
                    dataGridView1.Rows[projectNr].Cells[13].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (release == 0)
                    dataGridView1.Rows[projectNr].Cells[14].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (release == 1)
                    dataGridView1.Rows[projectNr].Cells[14].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (release == 2)
                    dataGridView1.Rows[projectNr].Cells[14].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (release == 3)
                    dataGridView1.Rows[projectNr].Cells[14].Style.BackColor = Color.FromArgb(255, 179, 186); //red
            }
        }
    }
}
/**********************************************
      *  RGB Colors Reference *
      *  light:
          "red": "255,179,186",
          "dark purple": "146,29,166",
          "orange": "255,223,186",
          "yellow": "255,255,186",
          "green": "186,255,201",
          "blue": "186,225,255",
          "white": "255,255,255",
          "dark blue": "45,85,151",
          "UI blue": "0,85,170",
          "grey": "225,225,225",
          "black": "0,0,0",
          "purple": "197,176,237"
      *  dark:
          "red": "212,0,15",
          "dark purple": "146,29,166",
          "orange": "247,99,12",
          "yellow": "235,165,0",
          "green": "16,137,62",
          "blue": "0,120,215",
          "white": "255,255,255",
          "dark blue": "45,85,151",
          "UI blue": "0,85,170",
          "grey": "93,90,88",
          "black": "0,0,0",
          "purple": "154,0,137"
      *********************************************/
