﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RefurbishmentManagement
{
    public partial class CT : MetroFramework.Forms.MetroForm
    {
        DateTimePicker dateTimePicker1 = new DateTimePicker();
        ComboBox comboBox = new ComboBox();
        Rectangle cellRect;
        DataTable dtable = new DataTable();
        int clickedRow = -1;
        int clickedColumn = -1;

        public CT()
        {
            InitializeComponent();
            dataGridView1.Controls.Add(dateTimePicker1);
            dataGridView1.Controls.Add(comboBox);

            comboBox.Items.Add("Expected");
            comboBox.Items.Add("Started");
            comboBox.Items.Add("Finished");
            comboBox.Items.Add("Delayed");
            comboBox.Visible = false;
            dateTimePicker1.Visible = false;
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            dateTimePicker1.TextChanged += new EventHandler(dtp_TextChange);
            comboBox.SelectedValueChanged += new EventHandler(cmb_TextChange);
        }
        private void dtp_TextChange(object sender, EventArgs e)
        {
            dataGridView1.CurrentCell.Value = dateTimePicker1.Text.ToString();
        }

        private void cmb_TextChange(object sender, EventArgs e)
        {
            if (comboBox.SelectedIndex == 0)
            {
                dataGridView1.CurrentCell.Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                //here write to the text file
                ExportToTextFile(0);
            }
            if (comboBox.SelectedIndex == 1)
            {
                dataGridView1.CurrentCell.Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                ExportToTextFile(1);
            }
            if (comboBox.SelectedIndex == 2)
            {
                dataGridView1.CurrentCell.Style.BackColor = Color.FromArgb(186, 255, 201); //green
                ExportToTextFile(2);
            }
            if (comboBox.SelectedIndex == 3)
            {
                dataGridView1.CurrentCell.Style.BackColor = Color.FromArgb(255, 179, 186); //red
                ExportToTextFile(3);
            }
            comboBox.Visible = false;
        }
        private void ExportToTextFile(int selectedDate)
        {
            string filename = dataGridView1.Rows[clickedRow].Cells[0].Value.ToString();
            var text = new StringBuilder();

            foreach (string s in File.ReadAllLines($"../../{filename}.txt"))
            {
                if (clickedColumn == 3 && selectedDate == 0)
                {
                    var rx = new Regex(@"Stripping_e=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Stripping_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Stripping_s=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Stripping_s=");
                    var rx3 = new Regex(@"Stripping_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Stripping_f=");
                    var rx4 = new Regex(@"Stripping_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Stripping_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 3 && selectedDate == 1)
                {
                    var rx = new Regex(@"Stripping_s=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Stripping_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Stripping_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Stripping_e=");
                    var rx3 = new Regex(@"Stripping_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Stripping_f=");
                    var rx4 = new Regex(@"Stripping_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Stripping_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 3 && selectedDate == 2)
                {
                    var rx = new Regex(@"Stripping_f=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Stripping_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Stripping_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Stripping_e=");
                    var rx3 = new Regex(@"Stripping_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Stripping_s=");
                    var rx4 = new Regex(@"Stripping_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Stripping_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 3 && selectedDate == 3)
                {
                    var rx = new Regex(@"Stripping_d=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Stripping_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Stripping_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Stripping_e=");
                    var rx3 = new Regex(@"Stripping_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Stripping_s=");
                    var rx4 = new Regex(@"Stripping_f=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Stripping_f=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 4 && selectedDate == 0)
                {
                    var rx = new Regex(@"Cleaning_e=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Cleaning_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Cleaning_s=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Cleaning_s=");
                    var rx3 = new Regex(@"Cleaning_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Cleaning_f=");
                    var rx4 = new Regex(@"Cleaning_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Cleaning_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 4 && selectedDate == 1)
                {
                    var rx = new Regex(@"Cleaning_s=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Cleaning_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Cleaning_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Cleaning_e=");
                    var rx3 = new Regex(@"Cleaning_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Cleaning_f=");
                    var rx4 = new Regex(@"Cleaning_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Cleaning_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 4 && selectedDate == 2)
                {
                    var rx = new Regex(@"Cleaning_f=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Cleaning_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Cleaning_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Cleaning_e=");
                    var rx3 = new Regex(@"Cleaning_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Cleaning_s=");
                    var rx4 = new Regex(@"Cleaning_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Cleaning_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 4 && selectedDate == 3)
                {
                    var rx = new Regex(@"Cleaning_d=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Cleaning_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Cleaning_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Cleaning_e=");
                    var rx3 = new Regex(@"Cleaning_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Cleaning_s=");
                    var rx4 = new Regex(@"Cleaning_f=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Cleaning_f=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 5 && selectedDate == 0)
                {
                    var rx = new Regex(@"Paint_e=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Paint_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Paint_s=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Paint_s=");
                    var rx3 = new Regex(@"Paint_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Paint_f=");
                    var rx4 = new Regex(@"Paint_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Paint_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 5 && selectedDate == 1)
                {
                    var rx = new Regex(@"Paint_s=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Paint_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Paint_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Paint_e=");
                    var rx3 = new Regex(@"Paint_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Paint_f=");
                    var rx4 = new Regex(@"Paint_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Paint_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 5 && selectedDate == 2)
                {
                    var rx = new Regex(@"Paint_f=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Paint_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Paint_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Paint_e=");
                    var rx3 = new Regex(@"Paint_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Paint_s=");
                    var rx4 = new Regex(@"Paint_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Paint_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 5 && selectedDate == 3)
                {
                    var rx = new Regex(@"Paint_d=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Paint_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Paint_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Paint_e=");
                    var rx3 = new Regex(@"Paint_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Paint_s=");
                    var rx4 = new Regex(@"Paint_f=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Paint_f=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 6 && selectedDate == 0)
                {
                    var rx = new Regex(@"Patient_e=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Patient_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Patient_s=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Patient_s=");
                    var rx3 = new Regex(@"Patient_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Patient_f=");
                    var rx4 = new Regex(@"Patient_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Patient_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 6 && selectedDate == 1)
                {
                    var rx = new Regex(@"Patient_s=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Patient_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Patient_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Patient_e=");
                    var rx3 = new Regex(@"Patient_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Patient_f=");
                    var rx4 = new Regex(@"Patient_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Patient_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 6 && selectedDate == 2)
                {
                    var rx = new Regex(@"Patient_f=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Patient_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Patient_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Patient_e=");
                    var rx3 = new Regex(@"Patient_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Patient_s=");
                    var rx4 = new Regex(@"Patient_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Patient_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 6 && selectedDate == 3)
                {
                    var rx = new Regex(@"Patient_d=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Patient_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Patient_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Patient_e=");
                    var rx3 = new Regex(@"Patient_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Patient_s=");
                    var rx4 = new Regex(@"Patient_f=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Patient_f=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 7 && selectedDate == 0)
                {
                    var rx = new Regex(@"Support_e=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Support_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Support_s=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Support_s=");
                    var rx3 = new Regex(@"Support_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Support_f=");
                    var rx4 = new Regex(@"Support_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Support_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 7 && selectedDate == 1)
                {
                    var rx = new Regex(@"Support_s=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Support_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Support_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Support_e=");
                    var rx3 = new Regex(@"Support_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Support_f=");
                    var rx4 = new Regex(@"Support_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Support_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 7 && selectedDate == 2)
                {
                    var rx = new Regex(@"Support_f=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Support_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Support_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Support_e=");
                    var rx3 = new Regex(@"Support_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Support_s=");
                    var rx4 = new Regex(@"Support_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Support_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 7 && selectedDate == 3)
                {
                    var rx = new Regex(@"Support_d=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Support_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Support_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Support_e=");
                    var rx3 = new Regex(@"Support_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Support_s=");
                    var rx4 = new Regex(@"Support_f=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Support_f=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 8 && selectedDate == 0)
                {
                    var rx = new Regex(@"Gantry_e=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Gantry_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Gantry_s=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Gantry_s=");
                    var rx3 = new Regex(@"Gantry_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Gantry_f=");
                    var rx4 = new Regex(@"Gantry_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Gantry_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 8 && selectedDate == 1)
                {
                    var rx = new Regex(@"Gantry_s=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Gantry_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Gantry_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Gantry_e=");
                    var rx3 = new Regex(@"Gantry_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Gantry_f=");
                    var rx4 = new Regex(@"Gantry_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Gantry_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 8 && selectedDate == 2)
                {
                    var rx = new Regex(@"Gantry_f=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Gantry_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Gantry_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Gantry_e=");
                    var rx3 = new Regex(@"Gantry_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Gantry_s=");
                    var rx4 = new Regex(@"Gantry_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Gantry_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 8 && selectedDate == 3)
                {
                    var rx = new Regex(@"Gantry_d=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Gantry_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Gantry_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Gantry_e=");
                    var rx3 = new Regex(@"Gantry_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Gantry_s=");
                    var rx4 = new Regex(@"Gantry_f=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Gantry_f=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 9 && selectedDate == 0)
                {
                    var rx = new Regex(@"Test_e=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Test_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Test_s=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Test_s=");
                    var rx3 = new Regex(@"Test_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Test_f=");
                    var rx4 = new Regex(@"Test_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Test_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 9 && selectedDate == 1)
                {
                    var rx = new Regex(@"Test_s=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Test_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Test_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Test_e=");
                    var rx3 = new Regex(@"Test_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Test_f=");
                    var rx4 = new Regex(@"Test_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Test_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 9 && selectedDate == 2)
                {
                    var rx = new Regex(@"Test_f=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Test_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Test_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Test_e=");
                    var rx3 = new Regex(@"Test_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Test_s=");
                    var rx4 = new Regex(@"Test_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Test_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 9 && selectedDate == 3)
                {
                    var rx = new Regex(@"Test_d=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Test_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Test_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Test_e=");
                    var rx3 = new Regex(@"Test_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Test_s=");
                    var rx4 = new Regex(@"Test_f=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Test_f=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 10 && selectedDate == 0)
                {
                    var rx = new Regex(@"Prepack_e=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Prepack_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Prepack_s=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Prepack_s=");
                    var rx3 = new Regex(@"Prepack_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Prepack_f=");
                    var rx4 = new Regex(@"Prepack_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Prepack_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 10 && selectedDate == 1)
                {
                    var rx = new Regex(@"Prepack_s=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Prepack_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Prepack_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Prepack_e=");
                    var rx3 = new Regex(@"Prepack_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Prepack_f=");
                    var rx4 = new Regex(@"Prepack_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Prepack_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 10 && selectedDate == 2)
                {
                    var rx = new Regex(@"Prepack_f=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Prepack_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Prepack_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Prepack_e=");
                    var rx3 = new Regex(@"Prepack_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Prepack_s=");
                    var rx4 = new Regex(@"Prepack_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Prepack_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 10 && selectedDate == 3)
                {
                    var rx = new Regex(@"Prepack_d=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Prepack_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Prepack_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Prepack_e=");
                    var rx3 = new Regex(@"Prepack_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Prepack_s=");
                    var rx4 = new Regex(@"Prepack_f=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Prepack_f=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 11 && selectedDate == 0)
                {
                    var rx = new Regex(@"Endpack_e=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Endpack_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Endpack_s=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Endpack_s=");
                    var rx3 = new Regex(@"Endpack_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Endpack_f=");
                    var rx4 = new Regex(@"Endpack_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Endpack_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 11 && selectedDate == 1)
                {
                    var rx = new Regex(@"Endpack_s=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Endpack_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Endpack_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Endpack_e=");
                    var rx3 = new Regex(@"Endpack_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Endpack_f=");
                    var rx4 = new Regex(@"Endpack_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Endpack_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 11 && selectedDate == 2)
                {
                    var rx = new Regex(@"Endpack_f=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Endpack_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Endpack_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Endpack_e=");
                    var rx3 = new Regex(@"Endpack_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Endpack_s=");
                    var rx4 = new Regex(@"Endpack_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Endpack_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 11 && selectedDate == 3)
                {
                    var rx = new Regex(@"Endpack_d=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Endpack_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Endpack_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Endpack_e=");
                    var rx3 = new Regex(@"Endpack_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Endpack_s=");
                    var rx4 = new Regex(@"Endpack_f=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Endpack_f=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 12 && selectedDate == 0)
                {
                    var rx = new Regex(@"Release_e=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Release_e={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Release_s=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Release_s=");
                    var rx3 = new Regex(@"Release_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Release_f=");
                    var rx4 = new Regex(@"Release_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Release_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 12 && selectedDate == 1)
                {
                    var rx = new Regex(@"Release_s=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Release_s={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Release_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Release_e=");
                    var rx3 = new Regex(@"Release_f=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Release_f=");
                    var rx4 = new Regex(@"Release_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Release_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 12 && selectedDate == 2)
                {
                    var rx = new Regex(@"Release_f=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Release_f={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Release_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Release_e=");
                    var rx3 = new Regex(@"Release_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Release_s=");
                    var rx4 = new Regex(@"Release_d=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Release_d=");
                    text.AppendLine(newText);
                }
                else if (clickedColumn == 12 && selectedDate == 3)
                {
                    var rx = new Regex(@"Release_d=(\d+/\d+/\d+)?");
                    string newText = rx.Replace(s, $"Release_d={dataGridView1.Rows[clickedRow].Cells[clickedColumn].Value.ToString()}");
                    var rx2 = new Regex(@"Release_e=(\d+/\d+/\d+)?");
                    newText = rx2.Replace(newText, $"Release_e=");
                    var rx3 = new Regex(@"Release_s=(\d+/\d+/\d+)?");
                    newText = rx3.Replace(newText, $"Release_s=");
                    var rx4 = new Regex(@"Release_f=(\d+/\d+/\d+)?");
                    newText = rx4.Replace(newText, $"Release_f=");
                    text.AppendLine(newText);
                }
            }

            using (var file = new StreamWriter(File.Create($"../../{filename}.txt")))
            {
                file.Write(text.ToString());
            }
        }
        private void btnTurnBack_Click(object sender, EventArgs e)
        {
            GeneralOverview overview = new GeneralOverview();
            overview.Show();
            this.Visible = false;
        }

        private void CT_Load(object sender, EventArgs e)
        {
            dtable.Columns.Add("SalesNr", typeof(int));
            dtable.Columns.Add("PName", typeof(string));
            dtable.Columns.Add("Type", typeof(string));
            dtable.Columns.Add("Stripping", typeof(string));
            dtable.Columns.Add("Cleaning", typeof(string));
            dtable.Columns.Add("Paint", typeof(string));
            dtable.Columns.Add("Patient", typeof(string));
            dtable.Columns.Add("Support", typeof(string));
            dtable.Columns.Add("Gantry", typeof(string));
            dtable.Columns.Add("Test", typeof(string));
            dtable.Columns.Add("Pre-pack", typeof(string));
            dtable.Columns.Add("End-pack", typeof(string));
            dtable.Columns.Add("Release for Delivery", typeof(string));
            dataGridView1.DataSource = dtable;
            List<string> projects = GetCTProjects();
            if (projects != null)
            {
                int nr = 0;
                foreach (string s in projects)
                {
                    Import(s, nr);
                    nr++;
                }
            }
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            WindowState = FormWindowState.Maximized;
        }
        private void Import(string fileName, int projectNr)
        {
            // get lines from the text file
            string[] lines = File.ReadAllLines($"../../{fileName}.txt");
            string[] values;

            for (int i = 0; i < lines.Length; i++)
            {
                values = lines[i].ToString().Split(',', '=');
                string[] row = new string[13];
                int stripping = -1;
                int cleaning = -1;
                int paint = -1;
                int patient = -1;
                int support = -1;
                int gantry = -1;
                int test = -1;
                int prepack = -1;
                int endpack = -1;
                int release = -1;

                for (int j = 0; j < values.Length; j++)
                {
                    row[0] = values[0].Trim();
                    row[1] = values[1].Trim();
                    row[2] = values[3].Trim();
                    if (values[j] == "Stripping_e")
                    {
                        if (values[j + 1].Trim() != "")
                        {
                            row[3] = values[j + 1].Trim();
                            stripping = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Stripping_s")
                    {
                        if (values[j + 1].Trim() != "")
                        {
                            row[3] = values[j + 1].Trim();
                            stripping = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Stripping_f")
                    {
                        if (values[j + 1].Trim() != "")
                        {
                            row[3] = values[j + 1].Trim();
                            stripping = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Stripping_d")
                    {
                        if (values[j + 1].Trim() != "")
                        {
                            row[3] = values[j + 1].Trim();
                            stripping = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Cleaning_e")
                    {
                        if (values[j + 1].Trim() != "")
                        {
                            row[4] = values[j + 1].Trim();
                            cleaning = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Cleaning_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[4] = values[j + 1].Trim();
                            cleaning = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Cleaning_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[4] = values[j + 1].Trim();
                            cleaning = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Cleaning_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[4] = values[j + 1].Trim();
                            cleaning = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Paint_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[5] = values[j + 1].Trim();
                            paint = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Paint_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[5] = values[j + 1].Trim();
                            paint = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Paint_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[5] = values[j + 1].Trim(); paint = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Paint_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[5] = values[j + 1].Trim(); paint = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Patient_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[6] = values[j + 1].Trim(); patient = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Patient_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[6] = values[j + 1].Trim(); patient = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Patient_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[6] = values[j + 1].Trim(); patient = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Patient_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[6] = values[j + 1].Trim(); patient = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Support_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[7] = values[j + 1].Trim(); support = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Support_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[7] = values[j + 1].Trim(); support = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Support_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[7] = values[j + 1].Trim(); support = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Support_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[7] = values[j + 1].Trim(); support = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Gantry_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[8] = values[j + 1].Trim(); gantry = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Gantry_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[8] = values[j + 1].Trim(); gantry = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Gantry_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[8] = values[j + 1].Trim(); gantry = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Gantry_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[8] = values[j + 1].Trim(); gantry = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Test_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[9] = values[j + 1].Trim(); test = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Test_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[9] = values[j + 1].Trim(); test = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Test_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[9] = values[j + 1].Trim(); test = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Test_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[9] = values[j + 1].Trim(); test = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Prepack_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[10] = values[j + 1].Trim(); prepack = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Prepack_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[10] = values[j + 1].Trim(); prepack = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Prepack_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[10] = values[j + 1].Trim(); prepack = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Prepack_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[10] = values[j + 1].Trim(); prepack = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Endpack_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[11] = values[j + 1].Trim(); endpack = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Endpack_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[11] = values[j + 1].Trim(); endpack = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Endpack_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[11] = values[j + 1].Trim(); endpack = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Endpack_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[11] = values[j + 1].Trim(); endpack = 3;
                        }
                        j++;
                    }
                    else if (values[j] == "Release_e")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[12] = values[j + 1].Trim(); release = 0;
                        }
                        j++;
                    }
                    else if (values[j] == "Release_s")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[12] = values[j + 1].Trim(); release = 1;
                        }
                        j++;
                    }
                    else if (values[j] == "Release_f")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[12] = values[j + 1].Trim(); release = 2;
                        }
                        j++;
                    }
                    else if (values[j] == "Release_d")
                    {
                        if (values[j + 1].Trim() != "") //check if there is a date entered
                        {
                            row[12] = values[j + 1].Trim(); release = 3;
                        }
                        j++;
                    }
                }
                dtable.Rows.Add(row);

                if (stripping == 0)
                    dataGridView1.Rows[projectNr].Cells[3].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (stripping == 1)
                    dataGridView1.Rows[projectNr].Cells[3].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (stripping == 2)
                    dataGridView1.Rows[projectNr].Cells[3].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (stripping == 3)
                    dataGridView1.Rows[projectNr].Cells[3].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (cleaning == 0)
                    dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (cleaning == 1)
                    dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (cleaning == 2)
                    dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (cleaning == 3)
                    dataGridView1.Rows[projectNr].Cells[4].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (paint == 0)
                    dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (paint == 1)
                    dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (paint == 2)
                    dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (paint == 3)
                    dataGridView1.Rows[projectNr].Cells[5].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (patient == 0)
                    dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (patient == 1)
                    dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (patient == 2)
                    dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (patient == 3)
                    dataGridView1.Rows[projectNr].Cells[6].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (support == 0)
                    dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (support == 1)
                    dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (support == 2)
                    dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (support == 3)
                    dataGridView1.Rows[projectNr].Cells[7].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (gantry == 0)
                    dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (gantry == 1)
                    dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (gantry == 2)
                    dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (gantry == 3)
                    dataGridView1.Rows[projectNr].Cells[8].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (test == 0)
                    dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (test == 1)
                    dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (test == 2)
                    dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (test == 3)
                    dataGridView1.Rows[projectNr].Cells[9].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (prepack == 0)
                    dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (prepack == 1)
                    dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (prepack == 2)
                    dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (prepack == 3)
                    dataGridView1.Rows[projectNr].Cells[10].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (endpack == 0)
                    dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (endpack == 1)
                    dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (endpack == 2)
                    dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (endpack == 3)
                    dataGridView1.Rows[projectNr].Cells[11].Style.BackColor = Color.FromArgb(255, 179, 186); //red
                if (release == 0)
                    dataGridView1.Rows[projectNr].Cells[12].Style.BackColor = Color.FromArgb(225, 225, 225); //grey
                else if (release == 1)
                    dataGridView1.Rows[projectNr].Cells[12].Style.BackColor = Color.FromArgb(255, 255, 186); //yellow
                else if (release == 2)
                    dataGridView1.Rows[projectNr].Cells[12].Style.BackColor = Color.FromArgb(186, 255, 201); //green
                else if (release == 3)
                    dataGridView1.Rows[projectNr].Cells[12].Style.BackColor = Color.FromArgb(255, 179, 186); //red
            }
        }
        private List<string> GetCTProjects()
        {
            string[] lines = File.ReadAllLines(@"..\..\AllRefurbishment.txt");
            string[] values;
            string[] row = null;
            List<string> allProjects = new List<string>();

            for (int i = 0; i < lines.Length; i++)
            {
                values = lines[i].ToString().Split(',');
                row = new string[values.Length];

                for (int j = 0; j < values.Length; j++)
                {
                    if (values[j] == "CT")
                    {
                        row[j] = values[j + 1].Trim();
                        allProjects.Add(row[j]);
                    }
                }
            }
            return allProjects;
        }
        private void CT_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1) return;
            if (dataGridView1.Columns[e.ColumnIndex].Name.CompareTo("SalesNr") != 0 && dataGridView1.Columns[e.ColumnIndex].Name.CompareTo("PName") != 0 &&
                 dataGridView1.Columns[e.ColumnIndex].Name.CompareTo("Type") != 0 && e.RowIndex > -1)
            {
                clickedRow = e.RowIndex;
                clickedColumn = e.ColumnIndex;
                cellRect = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                dateTimePicker1.Size = new Size(cellRect.Width, cellRect.Height);
                dateTimePicker1.Location = new Point(cellRect.X, cellRect.Y);
                dateTimePicker1.Visible = true;
            }
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            dateTimePicker1.Visible = false;
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            dateTimePicker1.Visible = false;
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            dateTimePicker1.Visible = false;
            cellRect = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
            comboBox.Size = new Size(cellRect.Width, cellRect.Height);
            comboBox.Location = new Point(cellRect.X, cellRect.Y);
            comboBox.Visible = true;
        }

        private void dataGridView1_CellStyleChanged(object sender, DataGridViewCellEventArgs e)
        {
            comboBox.Visible = false;
        }

        private void btnAddNewRecord_Click(object sender, EventArgs e)
        {
            AddOrUpdateRecord igt = new AddOrUpdateRecord(3);
            igt.Show();
            this.Visible = false;
        }
    }
}
